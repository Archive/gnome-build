/* gbf-backend.c
 *
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <libxml/tree.h>
#include "gbf-backend.h"
#include "glue-factory.h"

static GList *plugin_dirs = NULL;
static GSList *available_backends = NULL;
static GHashTable *backends_by_name = NULL;
static GlueFactory *glue_factory = NULL;


static void
destroy_backend (GbfBackend *backend)
{
	if (backend->id)
		g_free (backend->id);
	if (backend->name) 
		g_free (backend->name);

	g_free (backend);
}

static gboolean
str_has_suffix (const char *haystack,
		const char *needle)
{
	const char *h, *n;

	if (needle == NULL)
		return TRUE;

	if (haystack == NULL)
		return needle[0] == '\0';

	/* Eat one character at a time. */
	h = haystack + strlen(haystack);
	n = needle + strlen(needle);
	do {
		if (n == needle) {
			return TRUE;
		}
		if (h == haystack) {
			return FALSE;
		}
	} while (*--h == *--n);

	return FALSE;
}

static char *
get_attr (GHashTable *attrs,
	  const char *name,
	  GList      *lang_list)
{
#if 0
	GList *l;
	char *ret;

	for (l = lang_list; l != NULL; l = l->next) {
		char *lang_attr = g_strdup_printf ("%s-%s", 
						   name, 
						   (char*)l->data);
		ret = g_hash_table_lookup (attrs, lang_attr);
		g_free (lang_attr);

		if (ret)
			return ret;
	}
#endif

	return g_hash_table_lookup (attrs, name);
}

static GHashTable *
read_backend_attributes (xmlNodePtr backend)
{
	xmlNodePtr attr;
	GHashTable *attributes;

	attributes = g_hash_table_new_full (g_str_hash, g_str_equal, xmlFree, xmlFree);

	for (attr = backend->children; attr != NULL; attr = attr->next) {
		xmlChar *type = xmlGetProp (attr, BAD_CAST "type");

		if (type) {
			xmlChar *name;
			xmlChar *value;
			if (strcmp ((const char *) type, "string")) {
				g_warning ("gnome-build only supports string-type oaf attributes");
				return NULL;
			}

			name = xmlGetProp (attr, BAD_CAST "name");
			value = xmlGetProp (attr, BAD_CAST "value");

			if (name && value) {
				g_hash_table_insert (attributes, name, value);
			} else {
				g_warning ("Missing name or value in attribute");
			}
			xmlFree (type);
		}
	}

	return attributes;
}

static GbfBackend *
backend_from_attributes (xmlNodePtr  plugin,
			 GHashTable *attrs)
{
	static GList *langs = NULL;
	GbfBackend *backend = g_new0 (GbfBackend, 1);
	gboolean success = TRUE;
	char *str;

#if 0
	if (!langs)
		langs = anjuta_get_lang_list ();
#endif

	str = (char *) xmlGetProp (plugin, BAD_CAST "location");
	if (str) {
		backend->id = g_strdup (str);
		xmlFree (str);
	} else {
		g_warning ("Couldn't find 'location'");
		success = FALSE;
	}

	str = get_attr (attrs, "name", langs);
	if (str) {
		backend->name = g_strdup (str);
	} else {
		g_warning ("couldn't find 'name' attribute.");
		success = FALSE;
	}

	str = get_attr (attrs, "description", langs);
	if (str) {
		backend->description = g_strdup (str);
	} else {
		g_warning ("Couldn't find 'description' attribute.");
		success = FALSE;
	}

	if (!success) {
		destroy_backend (backend);
		backend = NULL;
	}

	return backend;
}

static void
load_backend (xmlNodePtr node)
{
	GHashTable *attrs;
	GbfBackend *backend;

	attrs = read_backend_attributes (node);

	backend = backend_from_attributes (node, attrs);
	if (backend) {
		if (g_hash_table_lookup (backends_by_name, backend->id)) {
			destroy_backend (backend);
		} else {
			available_backends = g_slist_prepend (available_backends,
							      backend);
			g_hash_table_insert (backends_by_name, backend->id, backend);
		}
	}

	g_hash_table_destroy (attrs);
}

static void
load_backend_file (const char *path)
{
	xmlDocPtr doc;
	xmlNodePtr root;
	xmlNodePtr backend;

	doc = xmlParseFile (path);

	if ((root = xmlDocGetRootElement (doc)) == NULL) {
		g_warning ("%s is not a valid backend description file", path);
	} else {
		for (backend = root->children; backend != NULL; backend = backend->next) {
			xmlChar *location = xmlGetProp (backend, BAD_CAST "location");
			if (location) {
				load_backend (backend);
				xmlFree (location);
			}
		}
	}

	xmlFreeDoc (doc);
}

static void
load_backends_from_directory (const char *dirname)
{
	DIR *dir;
	struct dirent *entry;

	dir = opendir (dirname);

	if (!dir)
		return;

	for (entry = readdir (dir); entry != NULL; entry = readdir (dir)) {
		if (str_has_suffix (entry->d_name, ".server")) {
			char *pathname;

			pathname = g_strdup_printf ("%s/%s", 
						    dirname, 
						    entry->d_name);

			load_backend_file (pathname);

			g_free (pathname);
		}
	}

	closedir (dir);
}

static void
load_available_backends (void)
{
	GList *l;
	backends_by_name = g_hash_table_new (g_str_hash, g_str_equal);

	for (l = plugin_dirs; l != NULL; l = l->next) {
		load_backends_from_directory ((char*)l->data);
	}
}

void
gbf_backend_init (void)
{
	static gboolean initialized = FALSE;
	const char *gnome2_path;
	char **pathv;
	char **p;

	if (initialized)
		return;

	initialized = TRUE;

	glue_factory = glue_factory_new ();

	gnome2_path = g_getenv ("GNOME2_PATH");
	if (gnome2_path) {
		pathv = g_strsplit (gnome2_path, ":", 1);
	
		for (p = pathv; *p != NULL; p++) {
			char *path = g_strdup (*p);
			plugin_dirs = g_list_prepend (plugin_dirs, path);
			glue_factory_add_path (glue_factory, path);
		}
		g_strfreev (pathv);
	}

	plugin_dirs = g_list_prepend (NULL, BACKEND_DIR);
	glue_factory_add_path (glue_factory, BACKEND_DIR);

	plugin_dirs = g_list_reverse (plugin_dirs);

	load_available_backends ();
}

GSList *
gbf_backend_get_backends (void)
{
	return available_backends;
}

GbfProject *
gbf_backend_new_project (const char *id)
{
	static GHashTable *types = NULL;
	GType type;
	GbfProject *project;

	if (!types) {
		types = g_hash_table_new (g_str_hash, g_str_equal);
	}

	type = GPOINTER_TO_UINT (g_hash_table_lookup (types, id));

	if (!type) {
		char **pieces;

		pieces = g_strsplit (id, ":", -1);
		type = glue_factory_get_object_type (glue_factory,
						     pieces[0],
						     pieces[1]);
		g_hash_table_insert (types,
				     g_strdup (id),
				     GUINT_TO_POINTER (type));

		g_strfreev (pieces);
	}

	if (type == G_TYPE_INVALID) {
		g_warning ("Invalid type\n");
		project = NULL;
	} else {
		project = GBF_PROJECT (g_object_new (type, NULL));
	}

	return project;
}
