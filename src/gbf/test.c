#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <libgnome/gnome-init.h>
#include "gbf-backend.h"

int main (int argc, char **argv)
{
	GSList *l;

	gnome_program_init ("libgbf-test", VERSION, LIBGNOME_MODULE, 
			    argc, argv, NULL,
			    NULL);  /* Avoid GCC sentinel warning */

	g_print ("initializing gbf backend...\n");

	gbf_backend_init ();

	g_print ("backends found:\n");

	for (l = gbf_backend_get_backends (); l; l = l->next) {
		GbfBackend *backend = l->data;

		g_print ("id         : %s\n", backend->id);
		g_print ("name       : %s\n", backend->name);
		g_print ("description: %s\n\n", backend->description);
	}

	g_print ("done\n");

	return 0;
}
