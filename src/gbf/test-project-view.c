#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <libbonoboui.h>
#include <libgnomevfs/gnome-vfs-uri.h>
#include <libgnomeui/gnome-ui-init.h>
#include <libgnome/gnome-program.h>

#include "gbf-i18n.h"
#include "gbf-backend.h"
#include "gbf-tree-data.h"
#include "gbf-project-view.h"
#include "gbf-project-model.h"
#include "gbf-project-util.h"

#define TEST_CONTROLS_UI_XML "./test-controls.xml"

static GtkWidget *app;
static GbfProject *proj = NULL;
static GtkWidget *view;
static GbfProjectModel *model;


static void open_cb      (GtkWidget *widget,
                          gpointer   user_data,
                          char      *cname);
static void open_project (GtkWidget *widget,
                          gpointer   user_data);
static void close_cb     (GtkWidget *widget,
			  gpointer   user_data,
			  char      *cname);
static void exit_cb      (GtkWidget *widget,
                          gpointer   user_data,
                          char      *cname);

static void group_new_cb  (BonoboUIComponent *component,
			   gpointer           user_data,
			   const gchar       *cname);
static void target_new_cb (BonoboUIComponent *component,
			   gpointer           user_data,
			   const gchar       *cname);
static void add_source_cb (BonoboUIComponent *component,
			   gpointer           user_data,
			   const gchar       *cname);




static BonoboUIVerb verbs[] = {
	BONOBO_UI_UNSAFE_VERB ("FileOpen", open_cb),
	BONOBO_UI_UNSAFE_VERB ("FileClose", close_cb),
	BONOBO_UI_UNSAFE_VERB ("FileExit", exit_cb),
	BONOBO_UI_VERB ("GroupNew", group_new_cb),
	BONOBO_UI_VERB ("TargetNew", target_new_cb),
	BONOBO_UI_VERB ("SourceAdd", add_source_cb),
	BONOBO_UI_VERB_END
};

static void 
group_new_cb (BonoboUIComponent *component,
	      gpointer           user_data,
	      const gchar       *cname)
{
    GtkWindow *win = user_data;
    
    gbf_project_util_new_group (model, win, NULL, "test-group-name");
}

static void 
target_new_cb (BonoboUIComponent *component,
	       gpointer           user_data,
	       const gchar       *cname)
{
    GtkWindow *win = user_data;

    gbf_project_util_new_target (model, win, NULL, "test-target-name");
}

static void 
add_source_cb (BonoboUIComponent *component,
	       gpointer           user_data,
	       const gchar       *cname)
{
    GtkWindow *win = user_data;
    
    gbf_project_util_add_source (model, win, NULL, NULL, NULL);
}


static void
open_cb (GtkWidget *widget, 
         gpointer   user_data, 
         char      *cname)
{
	GtkWidget *file_sel;

	file_sel = gtk_file_selection_new (_("Open Project"));

	g_signal_connect (GTK_FILE_SELECTION (file_sel)->ok_button,
			  "clicked",
			  G_CALLBACK (open_project),
			  file_sel);

	g_signal_connect_swapped (GTK_FILE_SELECTION (file_sel)->ok_button, 
				  "clicked", G_CALLBACK (gtk_widget_destroy),
				  file_sel);
	g_signal_connect_swapped (GTK_FILE_SELECTION (file_sel)->cancel_button, 
				  "clicked", G_CALLBACK (gtk_widget_destroy),
				  file_sel);

	gtk_widget_show (file_sel);
}

static void
show_packages (GbfProject* proj)
{
	GList* modules = gbf_project_get_config_modules(proj, NULL);
	GList* module;
	for (module = modules; module != NULL; module = g_list_next (module))
	{
		GList* packages = gbf_project_get_config_packages (proj, module->data, NULL);
		GList* package;
		printf ("%s\n", (gchar*)module->data);
		for (package = packages; package != NULL; package = g_list_next (package))
		{
			printf ("\t%s\n", (gchar*)package->data);
		}
		g_list_foreach (packages, (GFunc)g_free, NULL);
	}
	g_list_foreach (modules, (GFunc)g_free, NULL);
}

static void
open_project (GtkWidget *widget,
	      gpointer   user_data)
{
	GtkFileSelection *file_sel;
	const gchar *filename;
	GnomeVFSURI *vfs_uri;
	gchar *dirname;
	GSList *l;
	GbfBackend *backend = NULL;

	file_sel = GTK_FILE_SELECTION (user_data);    
	filename = gtk_file_selection_get_filename (file_sel);

	if (!filename)
		return;

	vfs_uri = gnome_vfs_uri_new (filename);
	dirname = gnome_vfs_uri_extract_dirname (vfs_uri);
	gnome_vfs_uri_unref (vfs_uri);

	if (proj != NULL)
		g_object_unref (proj);

	g_print ("initializing gbf backend...\n");
	gbf_backend_init ();

	for (l = gbf_backend_get_backends (); l; l = l->next) {
		backend = l->data;
		
		proj = gbf_backend_new_project (backend->id);
		if (proj)
		{
			if (gbf_project_probe (proj, dirname, NULL))
			{
				/* Backend found */
				break;
			}
			g_object_unref (proj);
			proj = NULL;
		}

		backend = NULL;
	}

	if (!backend) {
		g_print ("no appropriate backend available\n");
		return;
	}

	g_print ("created new %s project\n", backend->id);

	g_print ("loading project %s\n\n", dirname);
	/* FIXME: use the error parameter to determine if the project
	 * was loaded successfully */
	gbf_project_load (proj, dirname, NULL);
	g_free (dirname);
	
	show_packages (proj);
	
	g_object_set (G_OBJECT (model), "project", proj, NULL);
}

static void
close_cb (GtkWidget *widget,
	  gpointer   user_data,
	  char      *cname)
{
	if (proj) {
 		g_object_unref (proj);
		proj = NULL;
		g_object_set (G_OBJECT (model), "project", NULL, NULL);
	}
}

static void 
exit_cb (GtkWidget *widget, 
         gpointer   user_data, 
         char      *cname)
{
	gtk_widget_destroy (GTK_WIDGET (user_data));

	bonobo_main_quit ();
}

static void
window_destroyed (GtkWidget *window,
		  gpointer   user_data)
{
    BonoboUIComponent *ui_component;

    ui_component = g_object_get_data (G_OBJECT (window), "ui_component");
    if (ui_component) {
	bonobo_ui_component_unset_container (ui_component, NULL);
	bonobo_object_unref (ui_component);
	g_object_set_data (G_OBJECT (window), "ui_component", NULL);
    }
}

static void
uri_activated_cb (GtkWidget *widget, gchar *uri, gpointer user_data)
{
        g_message ("URI activated: %s", uri);
}

static BonoboWindow *
create_window (void)
{
	BonoboWindow       *win;
	BonoboUIContainer  *ui_container;
	BonoboUIComponent  *ui_component;
	GtkWidget          *scrolled_window;
	
	win = BONOBO_WINDOW (bonobo_window_new (GETTEXT_PACKAGE, _("Test Project View")));

	/* Create Container: */
	ui_container = bonobo_window_get_ui_container (win);

	/* This determines where the UI configuration info. will be stored */
	bonobo_ui_engine_config_set_path (bonobo_window_get_ui_engine (win),
					  "/apps/gnome-build/test-project/UIConfig/kvps");

	/* Create a UI component with which to communicate with the window */
	ui_component = bonobo_ui_component_new_default ();
	g_object_set_data (G_OBJECT (win), "ui_component", ui_component);
	
	/* Associate the BonoboUIComponent with the container */
	bonobo_ui_component_set_container (ui_component, 
					   BONOBO_OBJREF (ui_container), 
					   NULL);

	/* NB. this creates a relative file name from the current dir,
	 * in production you want to pass the application's datadir
	 * see Makefile.am to see how HELLO_SRCDIR gets set. */
	bonobo_ui_util_set_ui (ui_component, "", /* data dir */
	                       TEST_CONTROLS_UI_XML,
	                       "test-controls", NULL);

	/* Associate our verb -> callback mapping with the BonoboWindow */
	/* All the callback's user_data pointers will be set to 'win' */
	bonobo_ui_component_add_verb_list_with_data (ui_component, verbs, win);

        /* create model & view and bind them */
        model = gbf_project_model_new (NULL);
        view = gbf_project_view_new ();
        gtk_tree_view_set_model (GTK_TREE_VIEW (view), GTK_TREE_MODEL (model));
        g_object_unref (model);
        g_signal_connect (view, "uri-activated",
                          G_CALLBACK (uri_activated_cb), NULL);

        scrolled_window = gtk_scrolled_window_new (NULL, NULL);
        gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
                                        GTK_POLICY_AUTOMATIC,
                                        GTK_POLICY_AUTOMATIC);
        gtk_container_add (GTK_CONTAINER (scrolled_window), view);
        gtk_widget_show (view);
        
        bonobo_window_set_contents (win, scrolled_window);
        gtk_widget_show (scrolled_window);

	g_signal_connect (win, "destroy", G_CALLBACK (window_destroyed), NULL);
	g_signal_connect (win, "delete_event", G_CALLBACK (bonobo_main_quit), NULL);

	gtk_window_set_default_size (GTK_WINDOW (win), 500, 500);
    
	return win;
}

int
main (int argc, char *argv[])
{
    GOptionContext *option_context;
    
    option_context = g_option_context_new ("");
    
	gnome_program_init ("test-controls", VERSION, LIBGNOMEUI_MODULE, 
			    argc, argv, GNOME_PARAM_GOPTION_CONTEXT, option_context,
			    GNOME_PARAM_NONE);

	if (!bonobo_init (&argc, argv))
		g_error ("%s", _("Can't initialize bonobo!"));

	app = GTK_WIDGET (create_window ());
    
	gtk_widget_show_all (app);

	bonobo_main ();

	if (proj)
		g_object_unref (proj);

	bonobo_ui_debug_shutdown ();

	return 0;
}
