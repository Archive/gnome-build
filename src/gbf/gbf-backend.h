/* gbf-backend.h
 *
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _GBF_BACKEND_H_
#define _GBF_BACKEND_H_

#include <gbf/gbf-project.h>

G_BEGIN_DECLS

typedef struct _GbfBackend GbfBackend;

struct _GbfBackend {
	char *id;
	char *name;
	char *description;
};

void        gbf_backend_init         (void);
GSList     *gbf_backend_get_backends (void);
GbfProject *gbf_backend_new_project  (const char *id);

G_END_DECLS

#endif /* _GBF_BACKEND_H_ */
